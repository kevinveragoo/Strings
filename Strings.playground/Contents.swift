import Cocoa

// Create by Type Annotation
let name1: String
name1 = "Kevin"

// Create by Inference
let name = "Elisa"

let longGreeting = """
Hello!
How are you doing today?
"""

print(name)

// Count
print(name.count)

// Uppercased
print(name.uppercased())

// HasPrefix
print(longGreeting.hasPrefix("Hello"))

// Has Suffix
let profile = "Elisa.jpg"
print(profile.hasSuffix("jpg"))

// Combine Strings together with +
// This is slow as operators are executed serially
let firstPart = "Hello"
let secondPart = "Elisa"
let greeting = firstPart + secondPart
let greet = firstPart + " to you " + secondPart

// Combine Strings with Interpolation
let fastGreet = "\(firstPart) to you \(secondPart)"
let number = 2
let fastGreet2 = "\(number * 2) \(firstPart) \(secondPart)"
